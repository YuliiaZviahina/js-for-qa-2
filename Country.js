const { City } = require('./City.js');

class Country {
    arrayCity = [];

    constructor(name) {
        this.name = name;
    }

    appendCity(city) {
        if (city instanceof City)
            this.arrayCity.push(city);
    }

    removeCity(name) {
        let index = this.arrayCity.findIndex(c => c.name == name);
        if (index >= 0)
            this.arrayCity.splice(index, 1);
    }

}

module.exports = { Country }