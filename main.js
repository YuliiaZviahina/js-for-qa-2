const {City} = require('./City')
const {Country} = require('./Country.js');


var country = new Country('Russia');
country.appendCity(new City('Moscow');
country.appendCity(new City('Saint-Petersburg');
country.appendCity(new City('Novosibirsk');
country.appendCity(new City('Krasnodar');
country.appendCity(new City('Chelyabinsk');
country.appendCity(new City('Ekaterinburg');
country.appendCity(new City('Kaliningrad');
country.appendCity(new City('Sochi');
country.appendCity(new City('Samara');
country.appendCity(new City('Kazan');

country.cities.forEach(city => {
 city.setWeather().then(() => {console.log(city) }).catch((error) => { console.log(error) });
 city.setForecast().then(() => {console.log(city) }).catch((error) => { console.log(error) });
 });
